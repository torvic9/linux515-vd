## vd kernel 5.15

- support for clang
- support for module signing
- ProjectC (A. Chen) once released
- lrng (S. Müller)
- zstd (N. Terrell)
- futex2 (A. Almeida)
- block device LED trigger
- better Asus sensors support (nct6775)
- fixes, optimisations and backports

See PKGBUILD for source and more details.
